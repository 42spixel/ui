# **Ui**

Front-end part for spixel.

### **MIT License**

This work is licensed under the terms of **[MIT License](https://bitbucket.org/42spixel/ui/src/master/LICENSE.md)**.
