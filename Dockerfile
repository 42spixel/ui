FROM node:latest

LABEL mantainer="Gius. Camerlingo <gcamerli@gmail.com>"
LABEL version="1.0"
LABEL description="Front-end part for spixel."

# Docker image name
ENV NAME=ui

# Timezone
ENV TZ="Europe/Paris"

# Update system and install packages
RUN apt-get update 
RUN apt-get install -y --no-install-recommends \
	iputils-ping

# Clean system
RUN apt-get clean 
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Healthcheck
COPY healthcheck /usr/local/bin/
RUN chmod 744 /usr/local/bin/healthcheck

# User
RUN useradd -ms /bin/bash docker

# Home
ENV HOME=/home/docker

# Permissions
RUN chown -R docker:docker $HOME

# Change user
USER docker
